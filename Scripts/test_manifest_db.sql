attach '/tmp/test_manifest.db' AS 'test';

--  create_config_table in duplicity.manifest_v1 --
drop table if exists test.config;
create table if not exists test.config (
    backup_datetime timestamp primary key unique not null,
    code_version    text not null default "",
    data_version    text not null default "",
    hostname        text not null default "",
    fqdn            text not null default "",
    localdir        text not null default "",
    arguments       text not null default ""
);

--  create_diffdb_vols_table in duplicity.manifest_v1 --
drop table if exists test.diffdb_vols;
create table if not exists test.diffdb_vols (
    volume_number   integer primary key unique not null,
    start_index     integer not null default 0,
    end_index       integer not null default 0,
    start_block     integer not null default 0,
    end_block       integer not null default 0,
    hash_name       text not null default "",
    hash_value      text not null default "",
    foreign key(start_index) references files_changed(file_index),
    foreign key(end_index) references files_changed(file_index)
    );

--  create_files_changed_table in duplicity.manifest_v1 --
drop table if exists test.files_changed;
create table if not exists test.files_changed (
    file_index      integer primary key unique not null,
    pathname        text not null default "",
    change_type     text not null default ""
);

--  create_manifest_vols_table in duplicity.manifest_v1 --
drop table if exists test.manifest_vols;
create table if not exists test.manifest_vols (
    volume_number   integer primary key unique not null,
    start_index     integer not null default 0,
    end_index       integer not null default 0,
    start_block     integer not null default 0,
    end_block       integer not null default 0,
    hash_name       text not null default "",
    hash_value      text not null default "",
    foreign key(start_index) references files_changed(file_index),
    foreign key(end_index) references files_changed(file_index)
    );

--  create_sigdb_vols_table in duplicity.manifest_v1 --
drop table if exists test.sigdb_vols;
create table if not exists test.sigdb_vols (
    volume_number   integer primary key unique not null,
    start_index     integer not null default 0,
    end_index       integer not null default 0,
    start_block     integer not null default 0,
    end_block       integer not null default 0,
    hash_name       text not null default "",
    hash_value      text not null default "",
    foreign key(start_index) references files_changed(file_index),
    foreign key(end_index) references files_changed(file_index)
    );

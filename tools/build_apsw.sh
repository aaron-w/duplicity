#!/bin/bash

# This builds apsw which contains a duplicity specific
# version of sqlite3 linked statically.  This allows us
# to use the most recent sqlite instead of whatever
# ancient version is in the repo.

set -e -x

ROOTDIR=$(dirname $(realpath -e $0))/..

if [[ -e ${ROOTDIR}/apsw ]]; then

    cd ${ROOTDIR}/apsw

    # Build the static binary for import
    export CFLAGS="-Wno-unused-function -Wno-macro-redefined -Wno-sign-compare -Wno-deprecated-declarations \
                   -DSQLITE_TEMP_STORE=3 -DSQLITE_MAX_VARIABLE_NUMBER=250000 -DSQLITE_MAX_ATTACHED=125"
    python3 ./setup.py build_ext --inplace --enable-all-extensions build
    cp -v apsw*.so ${ROOTDIR}/duplicity

else

    # Grab the apsw source code.
    git clone git@github.com:rogerbinns/apsw.git
    rm -rf apsw/.git
    cd apsw

    # Build the static binary for import
    export CFLAGS="-Wno-unused-function -Wno-macro-redefined -Wno-sign-compare -Wno-deprecated-declarations \
                   -DSQLITE_TEMP_STORE=3 -DSQLITE_MAX_VARIABLE_NUMBER=250000 -DSQLITE_MAX_ATTACHED=125"
    python3 ./setup.py fetch --sqlite build_ext --inplace --enable-all-extensions test
    cp -v apsw*.so ${ROOTDIR}/duplicity

fi

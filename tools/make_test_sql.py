#!/usr/bin/env python3

import glob
import importlib
import inspect
import os
import sys


files = glob.glob(u'duplicity/*.py')
for module_name in [f[0:-3] for f in sorted(files)]:
    module_name = u'.'.join(module_name.split(u'/'))
    print(module_name)

    module = importlib.import_module(module_name)
    for name, data in inspect.getmembers(module):
         if name.startswith(u'create_') and isinstance(data, str):
             print(f'--  {name} in {module_name} --', file=sys.stdout)
             for line in [l.removeprefix(u'    ')  for l in data.split(u'\n')]:
                 print(line, file=sys.stdout)

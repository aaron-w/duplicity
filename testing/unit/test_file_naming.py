# -*- Mode:Python; indent-tabs-mode:nil; tab-width:4; encoding:utf8 -*-
#
# Copyright 2002 Ben Escoto <ben@emerose.org>
# Copyright 2007 Kenneth Loafman <kenneth@loafman.com>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

from __future__ import print_function
from builtins import object
from future import standard_library
standard_library.install_aliases()

import unittest

from duplicity import dup_time
from duplicity import file_naming
from duplicity import log
from duplicity import config
from duplicity import util
from testing.unit import UnitTestCase


class Test36(UnitTestCase):
    def test_base36(self):
        u"""Test conversion to/from base 36"""
        numlist = [0, 1, 10, 1313, 34233, 872338, 2342889,
                   134242234, 1204684368, 34972382455]
        for n in numlist:
            b = file_naming.to_base36(n)
            assert file_naming.from_base36(b) == n, (b, n)


class FileNamingBase:
    u"""Holds file naming test functions, for use in subclasses"""
    def test_basic(self):
        u"""Check get/parse cycle"""
        dup_time.setprevtime(10)
        dup_time.setcurtime(20)

        file_naming.prepare_regex(force=True)

        filename = file_naming.get(u"full-man", volume_number=23)
        log.Info(u"Full man filename: " + util.fsdecode(filename))
        pr = file_naming.parse(filename)
        assert pr and pr.filetype == u"full-man", pr
        assert pr.time == 20
        assert pr.volume_number == 23
        assert not pr.partial

        filename = file_naming.get(u"inc-man", volume_number=23)
        log.Info(u"Inc man filename: " + util.fsdecode(filename))
        pr = file_naming.parse(filename)
        assert pr and pr.filetype == u"inc-man", pr
        assert pr.start_time == 10
        assert pr.end_time == 20
        assert pr.volume_number == 23
        assert not pr.partial

        filename = file_naming.get(u"full-diff", volume_number=23)
        log.Info(u"Full diff filename: " + util.fsdecode(filename))
        pr = file_naming.parse(filename)
        assert pr and pr.filetype == u"full-diff", pr
        assert pr.time == 20
        assert pr.volume_number == 23
        assert not pr.partial

        filename = file_naming.get(u"inc-diff", volume_number=23)
        log.Info(u"Inc-diff filename: " + util.fsdecode(filename))
        pr = file_naming.parse(filename)
        assert pr and pr.filetype == u"inc-diff", pr
        assert pr.start_time == 10
        assert pr.end_time == 20
        assert pr.volume_number == 23
        assert not pr.partial

        filename = file_naming.get(u"full-sig", volume_number=23)
        log.Info(u"Full sig filename: " + util.fsdecode(filename))
        pr = file_naming.parse(filename)
        assert pr.filetype == u"full-sig"
        assert pr.time == 20
        assert pr.volume_number == 23
        assert not pr.partial

        filename = file_naming.get(u"inc-sig", volume_number=23)
        log.Info(u"Inc sig filename: " + util.fsdecode(filename))
        pr = file_naming.parse(filename)
        assert pr.filetype == u"inc-sig"
        assert pr.start_time == 10
        assert pr.end_time == 20
        assert pr.volume_number == 23
        assert not pr.partial

    def test_suffix(self):
        u"""Test suffix (encrypt/compressed) encoding and generation"""
        file_naming.prepare_regex(force=True)
        filename = file_naming.get(u"inc-man", volume_number=23, gzipped=1)
        pr = file_naming.parse(filename)
        assert pr and pr.compressed

        filename2 = file_naming.get(u"full-diff", volume_number=23, encrypted=1)
        pr = file_naming.parse(filename2)
        assert pr and pr.encrypted
        assert pr.volume_number == 23

    def test_more(self):
        u"""More file_parsing tests"""
        file_naming.prepare_regex(force=True)
        pr = file_naming.parse(config.file_prefix + config.file_prefix_signature + b"dns.h112bi.h14rg0.st.g")
        assert pr, pr
        assert pr.filetype == u"inc-sig"
        assert pr.end_time == 1029826800
        assert pr.volume_number == 1

        pr = file_naming.parse(config.file_prefix + config.file_prefix_signature + b"duplicity-new-signatures.2002-08-18T00:04:30-07:00.to.2002-08-20T00:00:00-07:00.sigtar.gpg")
        assert pr, pr
        assert pr.filetype == u"inc-sig"
        assert pr.end_time == 1029826800
        assert pr.volume_number == 1

        pr = file_naming.parse(config.file_prefix + config.file_prefix_signature + b"dfs.h5dixs.st.g")
        assert pr, pr
        assert pr.filetype == u"full-sig"
        assert pr.time == 1036954144, repr(pr.time)
        assert pr.volume_number == 1

    def test_partial(self):
        u"""Test addition of partial flag"""
        file_naming.prepare_regex(force=True)
        pr = file_naming.parse(config.file_prefix + config.file_prefix_signature + b"dns.h112bi.h14rg0.st.p.g")
        assert pr, pr
        assert pr.partial
        assert pr.filetype == u"inc-sig"
        assert pr.end_time == 1029826800
        assert pr.volume_number == 1

        pr = file_naming.parse(config.file_prefix + config.file_prefix_signature + b"duplicity-new-signatures.2002-08-18T00:04:30-07:00.to.2002-08-20T00:00:00-07:00.sigtar.part.gpg")
        assert pr, pr
        assert pr.partial
        assert pr.filetype == u"inc-sig"
        assert pr.end_time == 1029826800
        assert pr.volume_number == 1

        pr = file_naming.parse(config.file_prefix + config.file_prefix_signature + b"dfs.h5dixs.st.p.g")
        assert pr, pr
        assert pr.partial
        assert pr.filetype == u"full-sig"
        assert pr.time == 1036954144, repr(pr.time)
        assert pr.volume_number == 1


class FileNamingLong(UnitTestCase, FileNamingBase):
    u"""Test long filename parsing and generation"""
    def setUp(self):
        super(FileNamingLong, self).setUp()


class FileNamingPrefixes(UnitTestCase, FileNamingBase):
    u"""Test filename parsing and generation with prefixes"""
    def setUp(self):
        super(FileNamingPrefixes, self).setUp()
        self.set_config(u'file_prefix', b"global-")
        self.set_config(u'file_prefix_manifest', b"mani-")
        self.set_config(u'file_prefix_signature', b"sign-")
        self.set_config(u'file_prefix_archive', b"arch-")


if __name__ == u"__main__":
    from testing.start_pydevd import start_pydevd
    start_pydevd(remote=True)
    unittest.main()

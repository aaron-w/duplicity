# -*- Mode:Python; indent-tabs-mode:nil; tab-width:4; encoding:utf8 -*-
#
# Copyright 2020 Kenneth Loafman <kenneth@loafman.com>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
# See http://www.nongnu.org/duplicity for more information.
# Please send mail to me or the mailing list if you find bugs or have
# any suggestions.

import json
import os
import sys


def start_pydevd(remote=False):
    if (u'--pydevd' in sys.argv or
        os.getenv(u'PYDEVD', None) or
        globals().get(u'pydevd_running', None)):

        # In a dev environment the path is screwed so fix it.
        base = sys.path.pop(0)
        base = base.split(os.path.sep)[:-1]
        base = os.path.sep.join(base)
        sys.path.insert(0, base)

        if remote:
            # modify this for your configuration.
            # client_base = base path in machine that LiClipse is on
            # server_base = base path in machine that duplicity is on
            # client_name = fqdn of client
            # client_port = pydevd port
            client_base = u'/Users/ken/workspace/duplicity-sqlite'
            server_base = u'/home/ken/workspace/duplicity-sqlite'
            client_name = u'dione.local'
            client_port = 5678

            # relative paths under project root
            project_paths = [
                u'.',
                u'bin',
                u'duplicity',
                u'duplicity/backends',
                u'testing',
                u'testing/functional',
                u'testing/unit',
            ]
            pathlist = [(os.path.normpath(os.path.join(client_base, p)),
                         os.path.normpath(os.path.join(server_base, p)))
                         for p in project_paths]
            os.environ[u'PATHS_FROM_ECLIPSE_TO_PYTHON'] = json.dumps(pathlist)

            # start remote debug
            # do not move import since os.environ call above is required
            import pydevd  # pylint: disable=import-error
            pydevd.settrace(host=client_name, port=client_port, stdoutToServer=True, stderrToServer=True)

        else:
            # start local debug
            # do not move import since os.environ call above is required
            import pydevd  # pylint: disable=import-error
            pydevd.settrace()

        # flag running status
        globals()[u'pydevd_running'] = True

# -*- Mode:Python; indent-tabs-mode:nil; tab-width:4; encoding:utf8 -*-
#
# Copyright 2007 Kenneth Loafman <kenneth@loafman.com>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

u"""Create and edit manifest for session contents"""

import re
import sys
from datetime import datetime
from datetime import timezone

from duplicity import __version__ as code_version
from duplicity import apsw
from duplicity import config
from duplicity import log
from duplicity import util


config.data_version = u'1.0.0'

create_config_table = f'''\
    drop table if exists {config.schema}.config;
    create table if not exists {config.schema}.config (
        backup_datetime timestamp primary key unique not null,
        config          text not null default "",
        args            text not null default ""
    );
    '''

create_files_changed_table = f'''\
    drop table if exists {config.schema}.files_changed;
    create table if not exists {config.schema}.files_changed (
        file_index      integer primary key unique not null,
        pathname        text not null default "",
        change_type     text not null default ""
    );
    '''

create_manifest_vols_table = f'''\
    drop table if exists {config.schema}.manifest_vols;
    create table if not exists {config.schema}.manifest_vols (
        volume_number   integer primary key unique not null,
        start_index     integer not null default 0,
        end_index       integer not null default 0,
        start_block     integer not null default 0,
        end_block       integer not null default 0,
        hash_name       text not null default "",
        hash_value      text not null default "",
        foreign key(start_index) references files_changed(file_index),
        foreign key(end_index) references files_changed(file_index)
        );
    '''

create_diffdb_vols_table = f'''\
    drop table if exists {config.schema}.diffdb_vols;
    create table if not exists {config.schema}.diffdb_vols (
        volume_number   integer primary key unique not null,
        start_index     integer not null default 0,
        end_index       integer not null default 0,
        start_block     integer not null default 0,
        end_block       integer not null default 0,
        hash_name       text not null default "",
        hash_value      text not null default "",
        foreign key(start_index) references files_changed(file_index),
        foreign key(end_index) references files_changed(file_index)
        );
    '''

create_sigdb_vols_table = f'''\
    drop table if exists {config.schema}.sigdb_vols;
    create table if not exists {config.schema}.sigdb_vols (
        volume_number   integer primary key unique not null,
        start_index     integer not null default 0,
        end_index       integer not null default 0,
        start_block     integer not null default 0,
        end_block       integer not null default 0,
        hash_name       text not null default "",
        hash_value      text not null default "",
        foreign key(start_index) references files_changed(file_index),
        foreign key(end_index) references files_changed(file_index)
        );
    '''


class ManifestError(Exception):
    u"""
    Exception raised when problem with manifest
    """
    pass


class Manifest:
    u"""
    List of volumes and information about each one
    """
    def __init__(self, filename=None):
        u"""
        Create empty Manifest database

        @param filename: filename for manifest
        @type filename: bytes (optional)

        @rtype: Manifest
        @return: Manifest
        """
        self.filename = filename if filename else u":memory:"

        self.conn = apsw.Connection(self.filename)
        self.cursor = self.conn.cursor()
        self.cursor.execute(f"attach database '{self.filename}' as {config.schema}")
        self.create_manifest()
        log.Info(f"Manifest database created: {self.filename}")

    def create_manifest(self):
        for script in (create_config_table, create_files_changed_table, create_manifest_vols_table,
                       create_diffdb_vols_table, create_sigdb_vols_table):
            try:
                self.cursor.execute(script)
            except apsw.Error as e:
                log.FatalError(f"Error creating manifest table {script}: {e}")

    def set_config(self):
        u"""
        Set information about directory from config values

        @rtype: Manifest
        @return: Manifest
        """
        try:
            with self.conn:
                self.conn.execute(u"insert into config values(?,?,?,?);",
                                  data_version,
                                  datetime.now(tz=timezone.utc),
                                  config.dump_config(),
                                  json.dumps(sys.argv))
        except apsw.Error as e:
            log.FatalError(f"Error writing config: {e}")
        return self

    def check_config(self):
        u"""
        Return None if dirinfo is the same, otherwise error message

        Does not raise an error message if hostname or local_dirname
        are not available.

        @rtype: string
        @return: None or error message
        """
        if config.allow_source_mismatch:
            return

        if self.hostname and self.hostname != config.hostname:
            errmsg = _(u"Fatal Error: Backup source host has changed.\n"
                       u"Current hostname: %s\n"
                       u"Previous hostname: %s") % (config.hostname, self.hostname)
            code = log.ErrorCode.hostname_mismatch
            code_extra = u"%s %s" % (util.escape(config.hostname), util.escape(self.hostname))

        elif (self.local_dirname and self.local_dirname != config.local_path.name):
            errmsg = _(u"Fatal Error: Backup source directory has changed.\n"
                       u"Current directory: %s\n"
                       u"Previous directory: %s") % (config.local_path.name, self.local_dirname)
            code = log.ErrorCode.source_dir_mismatch
            code_extra = u"%s %s" % (util.escape(config.local_path.name),
                                     util.escape(self.local_dirname))
        else:
            return

        log.FatalError(errmsg + u"\n\n" +
                       _(u"Aborting because you may have accidentally tried to "
                         u"backup two different data sets to the same remote "
                         u"location, or using the same archive directory.  If "
                         u"this is not a mistake, use the "
                         u"--allow-source-mismatch switch to avoid seeing this "
                         u"message"), code, code_extra)

    def add_diffdb_volume_info(self, vi):
        u"""
        Add volume info vi to manifest and write to manifest

        @param vi: volume info to add
        @type vi: VolumeInfo

        @return: void
        """
        try:
            self.cursor.execute(u"insert into diffdb_vols values(?,?,?,?,?,?,?);",
                                vi.volume_number,
                                vi.start_index,
                                vi.start_block,
                                vi.end_index,
                                vi.end_block,
                                vi.hashes[0],
                                vi.hashes[1])
        except apsw.Error as e:
            log.FatalError(u"Error inserting diffdb: {}".format(e.args[0]))

    def del_diffdb_volume_info(self, vol_num):
        u"""
        Remove volume vol_num from the manifest

        @param vol_num: volume number to delete
        @type vi: int

        @return: void
        """
        try:
            with self.conn:
                self.conn.execute(u"delete diffdb_vols where volume_number = ?;", vol_num)
        except apsw.Error as e:
            log.FatalError(u"Error deleting diffdb info: {}".format(e.args[0]))

    def __eq__(self, other):
        u"""
        Two manifests are equal if they contain the same volume infos
        """
#         vi_list1 = list(self.volume_info_dict.keys())
#         vi_list1.sort()
#         vi_list2 = list(other.volume_info_dict.keys())
#         vi_list2.sort()
#
#         if vi_list1 != vi_list2:
#             log.Notice(_(u"Manifests not equal because different volume numbers"))
#             return False
#
#         for i in range(len(vi_list1)):
#             if not vi_list1[i] == vi_list2[i]:
#                 log.Notice(_(u"Manifests not equal because volume lists differ"))
#                 return False
#
#         if (self.hostname != other.hostname or
#                 self.local_dirname != other.local_dirname):
#             log.Notice(_(u"Manifests not equal because hosts or directories differ"))
#             return False

        return True

    def __ne__(self, other):
        u"""
        Defines !=.  Not doing this always leads to annoying bugs...
        """
        return not self.__eq__(other)

    def get_containing_volumes(self, index_prefix):
        u"""
        Return list of volume numbers that may contain index_prefix
        """
        if len(index_prefix) == 1 and isinstance(index_prefix[0], u"".__class__):
            index_prefix = (index_prefix[0].encode(),)
        return [vol_num for vol_num in list(self.volume_info_dict.keys()) if
                self.volume_info_dict[vol_num].contains(index_prefix)]


class VolumeInfoError(Exception):
    u"""
    Raised when there is a problem initializing a VolumeInfo
    """
    pass


class VolumeInfo:
    u"""
    Information about a single volume
    """
    def __init__(self):
        u"""VolumeInfo initializer"""
        self.volume_number = None
        self.start_index = None
        self.start_block = None
        self.end_index = None
        self.end_block = None
        self.hashes = {}

    def set_info(self, vol_number,
                 start_index, start_block,
                 end_index, end_block):
        u"""
        Set essential VolumeInfo information, return self

        Call with starting and ending paths stored in the volume.  If
        a multivol diff gets split between volumes, count it as being
        part of both volumes.
        """
        self.volume_number = vol_number
        self.start_index = start_index
        self.start_block = start_block
        self.end_index = end_index
        self.end_block = end_block

        return self

    def set_hash(self, hash_name, data):
        u"""
        Set the value of hash hash_name (e.g. "MD5") to data
        """
        if isinstance(hash_name, bytes):
            hash_name = hash_name.decode()
        if isinstance(data, bytes):
            data = data.decode()
        self.hashes[hash_name] = data

    def get_best_hash(self):
        u"""
        Return pair (hash_type, hash_data)

        SHA1 is the best hash, and MD5 is the second best hash.  None
        is returned if no hash is available.
        """
        if not self.hashes:
            return None
        try:
            return (u"SHA1", self.hashes[u'SHA1'])
        except KeyError:
            pass
        try:
            return (u"MD5", self.hashes[u'MD5'])
        except KeyError:
            pass
        return list(self.hashes.items())[0]

    def __eq__(self, other):
        u"""
        Used in test suite
        """
        if not isinstance(other, VolumeInfo):
            log.Notice(_(u"Other is not VolumeInfo"))
            return None
        if self.volume_number != other.volume_number:
            log.Notice(_(u"Volume numbers don't match"))
            return None
        if self.start_index != other.start_index:
            log.Notice(_(u"start_indicies don't match"))
            return None
        if self.end_index != other.end_index:
            log.Notice(_(u"end_index don't match"))
            return None
        hash_list1 = list(self.hashes.items())
        hash_list1.sort()
        hash_list2 = list(other.hashes.items())
        hash_list2.sort()
        if hash_list1 != hash_list2:
            log.Notice(_(u"Hashes don't match"))
            return None
        return 1

    def __ne__(self, other):
        u"""
        Defines !=
        """
        return not self.__eq__(other)

    def contains(self, index_prefix, recursive=1):
        u"""
        Return true if volume might contain index

        If recursive is true, then return true if any index starting
        with index_prefix could be contained.  Otherwise, just check
        if index_prefix itself is between starting and ending
        indicies.
        """
        if recursive:
            return (self.start_index[:len(index_prefix)] <=
                    index_prefix <= self.end_index)
        else:
            return self.start_index <= index_prefix <= self.end_index

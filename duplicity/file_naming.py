# -*- Mode:Python; indent-tabs-mode:nil; tab-width:4; encoding:utf8 -*-
#
# Copyright 2002 Ben Escoto <ben@emerose.org>
# Copyright 2007 Kenneth Loafman <kenneth@loafman.com>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

u"""Produce and parse the names of duplicity's backup files"""

from builtins import str
from builtins import range
from builtins import object

import re
import sys
from collections import namedtuple

from duplicity import dup_time
from duplicity import config

# new v1 filename format
full_manifest_re_v1 = None
full_vol_re_v1 = None
full_sig_re_v1 = None
inc_manifest_re_v1 = None
inc_vol_re_v1 = None
new_sig_re_v1 = None

# old filename format
full_manifest_re = None
full_vol_re = None
full_sig_re = None
inc_manifest_re = None
inc_vol_re = None
new_sig_re = None

# short filename format
full_manifest_re_short = None
full_vol_re_short = None
full_sig_re_short = None
inc_manifest_re_short = None
inc_vol_re_short = None
new_sig_re_short = None

# map regexes to file info
global file_mapping


def prepare_regex(force=False):
    # new v1 filename format
    global full_manifest_re_v1
    global full_vol_re_v1
    global full_sig_re_v1
    global inc_manifest_re_v1
    global inc_vol_re_v1
    global inc_sig_re_v1

    # original filename format
    global full_manifest_re
    global full_vol_re
    global full_sig_re
    global inc_manifest_re
    global inc_vol_re
    global new_sig_re

    # short filename format
    global full_manifest_re_short
    global full_vol_re_short
    global full_sig_re_short
    global inc_manifest_re_short
    global inc_vol_re_short
    global new_sig_re_short

    # map regexes to file info
    global file_mapping

    # we force regex re-generation in unit tests because file prefixes might have changed
    if full_vol_re and not force:
        return

    # ===================================================================================================================
    # new v1 filename format -- every filename has a volume number
    # ===================================================================================================================

    full_manifest_re_v1 = re.compile(b"^" + config.file_prefix + config.file_prefix_manifest + b"duplicity-full"
                                     b"\\.(?P<time>.*?)"
                                     b"\\.vol(?P<num>[0-9]+)"
                                     b"\\.mandb"
                                     b"(?P<partial>(\\.part))?"
                                     b"($|\\.)")

    full_vol_re_v1 = re.compile(b"^" + config.file_prefix + config.file_prefix_archive + b"duplicity-full"
                                b"\\.(?P<time>.*?)"
                                b"\\.vol(?P<num>[0-9]+)"
                                b"\\.diffdb"
                                b"(?P<partial>(\\.part))?"
                                b"($|\\.)")

    full_sig_re_v1 = re.compile(b"^" + config.file_prefix + config.file_prefix_signature + b"duplicity-full"
                                b"\\.(?P<time>.*?)"
                                b"\\.vol(?P<num>[0-9]+)"
                                b"\\.sigdb"
                                b"(?P<partial>(\\.part))?"
                                b"(\\.|$)")

    inc_manifest_re_v1 = re.compile(b"^" + config.file_prefix + config.file_prefix_manifest + b"duplicity-inc"
                                    b"\\.(?P<start_time>.*?)"
                                    b"\\.to"
                                    b"\\.(?P<end_time>.*?)"
                                    b"\\.vol(?P<num>[0-9]+)"
                                    b"\\.mandb"
                                    b"(?P<partial>(\\.part))?"
                                    b"(\\.|$)")

    inc_vol_re_v1 = re.compile(b"^" + config.file_prefix + config.file_prefix_archive + b"duplicity-inc"
                               b"\\.(?P<start_time>.*?)"
                               b"\\.to"
                               b"\\.(?P<end_time>.*?)"
                               b"\\.vol(?P<num>[0-9]+)"
                               b"\\.diffdb"
                               b"(?P<partial>(\\.part))?"
                               b"($|\\.)")

    inc_sig_re_v1 = re.compile(b"^" + config.file_prefix + config.file_prefix_signature + b"duplicity-inc"
                               b"\\.(?P<start_time>.*?)"
                               b"\\.to"
                               b"\\.(?P<end_time>.*?)"
                               b"\\.vol(?P<num>[0-9]+)"
                               b"\\.sigdb"
                               b"(?P<partial>(\\.part))?"
                               b"(\\.|$)")

    # ===================================================================================================================
    # original filename format -- as of 0.9 we do not write this format
    # ===================================================================================================================

    full_manifest_re = re.compile(b"^" + config.file_prefix + config.file_prefix_manifest + b"duplicity-full"
                                  b"\\.(?P<time>.*?)"
                                  b"\\.manifest"
                                  b"(?P<partial>(\\.part))?"
                                  b"($|\\.)")

    full_vol_re = re.compile(b"^" + config.file_prefix + config.file_prefix_archive + b"duplicity-full"
                             b"\\.(?P<time>.*?)"
                             b"\\.vol(?P<num>[0-9]+)"
                             b"\\.difftar"
                             b"(?P<partial>(\\.part))?"
                             b"($|\\.)")

    full_sig_re = re.compile(b"^" + config.file_prefix + config.file_prefix_signature + b"duplicity-full-signatures"
                             b"\\.(?P<time>.*?)"
                             b"\\.sigtar"
                             b"(?P<partial>(\\.part))?"
                             b"(\\.|$)")

    inc_manifest_re = re.compile(b"^" + config.file_prefix + config.file_prefix_manifest + b"duplicity-inc"
                                 b"\\.(?P<start_time>.*?)"
                                 b"\\.to"
                                 b"\\.(?P<end_time>.*?)"
                                 b"\\.manifest"
                                 b"(?P<partial>(\\.part))?"
                                 b"(\\.|$)")

    inc_vol_re = re.compile(b"^" + config.file_prefix + config.file_prefix_archive + b"duplicity-inc"
                            b"\\.(?P<start_time>.*?)"
                            b"\\.to\\.(?P<end_time>.*?)"
                            b"\\.vol(?P<num>[0-9]+)"
                            b"\\.difftar"
                            b"(?P<partial>(\\.part))?"
                            b"($|\\.)")

    new_sig_re = re.compile(b"^" + config.file_prefix + config.file_prefix_signature + b"duplicity-new-signatures"
                            b"\\.(?P<start_time>.*?)"
                            b"\\.to"
                            b"\\.(?P<end_time>.*?)"
                            b"\\.sigtar"
                            b"(?P<partial>(\\.part))?"
                            b"(\\.|$)")

    # ===================================================================================================================
    # short filename format -- as of 0.9 we do not write this format
    # ===================================================================================================================

    full_manifest_re_short = re.compile(b"^" + config.file_prefix + config.file_prefix_manifest + b"df"
                                        b"\\.(?P<time>[0-9a-z]+?)"
                                        b"\\.m"
                                        b"(?P<partial>(\\.p))?"
                                        b"($|\\.)")

    full_vol_re_short = re.compile(b"^" + config.file_prefix + config.file_prefix_archive + b"df"
                                   b"\\.(?P<time>[0-9a-z]+?)"
                                   b"\\.(?P<num>[0-9a-z]+)"
                                   b"\\.dt"
                                   b"(?P<partial>(\\.p))?"
                                   b"($|\\.)")

    full_sig_re_short = re.compile(b"^" + config.file_prefix + config.file_prefix_signature + b"dfs"
                                   b"\\.(?P<time>[0-9a-z]+?)"
                                   b"\\.st"
                                   b"(?P<partial>(\\.p))?"
                                   b"(\\.|$)")

    inc_manifest_re_short = re.compile(b"^" + config.file_prefix + config.file_prefix_manifest + b"di"
                                       b"\\.(?P<start_time>[0-9a-z]+?)"
                                       b"\\.(?P<end_time>[0-9a-z]+?)"
                                       b"\\.m"
                                       b"(?P<partial>(\\.p))?"
                                       b"(\\.|$)")

    inc_vol_re_short = re.compile(b"^" + config.file_prefix + config.file_prefix_archive + b"di"
                                  b"\\.(?P<start_time>[0-9a-z]+?)"
                                  b"\\.(?P<end_time>[0-9a-z]+?)"
                                  b"\\.(?P<num>[0-9a-z]+)"
                                  b"\\.dt"
                                  b"(?P<partial>(\\.p))?"
                                  b"($|\\.)")

    new_sig_re_short = re.compile(b"^" + config.file_prefix + config.file_prefix_signature + b"dns"
                                  b"\\.(?P<start_time>[0-9a-z]+?)"
                                  b"\\.(?P<end_time>[0-9a-z]+?)"
                                  b"\\.st"
                                  b"(?P<partial>(\\.p))?"
                                  b"(\\.|$)")

    RegexToFileType = namedtuple(u'RegexToFileType', [u'regex', u'filetype', u'short', u'multivol'])

    file_mapping = [
        # new v1 filename format
        RegexToFileType(regex=full_manifest_re_v1, filetype=u'full-man', short=False, multivol=True),
        RegexToFileType(regex=full_vol_re_v1, filetype=u'full-diff', short=False, multivol=True),
        RegexToFileType(regex=full_sig_re_v1, filetype=u'full-sig', short=False, multivol=True),
        RegexToFileType(regex=inc_manifest_re_v1, filetype=u'inc-man', short=False, multivol=True),
        RegexToFileType(regex=inc_vol_re_v1, filetype=u'inc-diff', short=False, multivol=True),
        RegexToFileType(regex=inc_sig_re_v1, filetype=u'inc-sig', short=False, multivol=True),

        # old filename format
        RegexToFileType(regex=full_manifest_re, filetype=u'full-man', short=False, multivol=False),
        RegexToFileType(regex=full_vol_re, filetype=u'full-diff', short=False, multivol=True),
        RegexToFileType(regex=full_sig_re, filetype=u'full-sig', short=False, multivol=False),
        RegexToFileType(regex=inc_manifest_re, filetype=u'inc-man', short=False, multivol=False),
        RegexToFileType(regex=inc_vol_re, filetype=u'inc-diff', short=False, multivol=True),
        RegexToFileType(regex=new_sig_re, filetype=u'inc-sig', short=False, multivol=False),

        # short filename format
        RegexToFileType(regex=full_manifest_re_short, filetype=u'full-man', short=True, multivol=False),
        RegexToFileType(regex=full_vol_re_short, filetype=u'full-diff', short=True, multivol=True),
        RegexToFileType(regex=full_sig_re_short, filetype=u'full-sig', short=True, multivol=False),
        RegexToFileType(regex=inc_manifest_re_short, filetype=u'inc-man', short=True, multivol=False),
        RegexToFileType(regex=inc_vol_re_short, filetype=u'inc-diff', short=True, multivol=True),
        RegexToFileType(regex=new_sig_re_short, filetype=u'inc-sig', short=True, multivol=False),
    ]


def to_base36(n):
    u"""
    Return string representation of n in base 36 (use 0-9 and a-z)
    """
    div, mod = divmod(n, 36)
    if mod <= 9:
        last_digit = str(mod)
    else:
        last_digit = chr(ord(u'a') + mod - 10)
    if sys.version_info.major >= 3:
        last_digit = last_digit.encode()
    if n == mod:
        return last_digit
    else:
        return to_base36(div) + last_digit


def from_base36(s):
    u"""
    Convert string s in base 36 to long int
    """
    total = 0
    for i in range(len(s)):
        total *= 36
        if sys.version_info.major >= 3 and isinstance(s, bytes):
            digit_ord = s[i]
        else:
            digit_ord = ord(s[i])
        if ord(u'0') <= digit_ord <= ord(u'9'):
            total += digit_ord - ord(u'0')
        elif ord(u'a') <= digit_ord <= ord(u'z'):
            total += digit_ord - ord(u'a') + 10
        else:
            assert 0, u"Digit %s in %s not in proper range" % (s[i], s)
    return total


def get_suffix(encrypted, gzipped):
    u"""
    Return appropriate suffix depending on status of
    encryption, compression, and short_filenames.
    """
    if encrypted:
        gzipped = False
    if encrypted:
        if config.short_filenames:
            suffix = b'.g'
        else:
            suffix = b".gpg"
    elif gzipped:
        if config.short_filenames:
            suffix = b".z"
        else:
            suffix = b'.gz'
    else:
        suffix = b""
    return suffix


def get(filetype, volume_number=None, encrypted=False, gzipped=False, partial=False):
    u"""
    Return duplicity filename of specified filetype

    filetype can be "full-man", "inc-man", "full-diff", "inc-diff", "full-sig", "inc-sig".
    volume_number is given with all v1 format files.
    """
    assert volume_number is not None, u"Invalid volume number."
    if encrypted:
        gzipped = False
    suffix = get_suffix(encrypted, gzipped)
    part_string = b".part" if partial else b""

    if filetype == u"full-diff":
        return (config.file_prefix + config.file_prefix_archive +
                b"duplicity-full.%s.vol%d.diffdb.%s%s" %
                (dup_time.curtimestr.encode(),
                 volume_number, part_string, suffix))

    elif filetype == u"inc-diff":
        return (config.file_prefix + config.file_prefix_archive +
                b"duplicity-inc.%s.to.%s.vol%d.diffdb.%s%s" %
                (dup_time.prevtimestr.encode(),
                 dup_time.curtimestr.encode(),
                 volume_number, part_string, suffix))

    elif filetype == u"full-sig":
        return (config.file_prefix + config.file_prefix_signature +
                b"duplicity-full.%s.vol%d.sigdb.%s%s" %
                (dup_time.curtimestr.encode(), volume_number, part_string, suffix))

    elif filetype == u"inc-sig":
        return (config.file_prefix + config.file_prefix_signature +
                b"duplicity-inc.%s.to.%s.vol%d.sigdb%s%s" %
                (dup_time.prevtimestr.encode(), dup_time.curtimestr.encode(),
                 volume_number, part_string, suffix))

    elif filetype == u"full-man":
        return (config.file_prefix + config.file_prefix_manifest +
                b"duplicity-full.%s.vol%d.mandb.%s%s" %
                (dup_time.curtimestr.encode(), volume_number, part_string, suffix))

    elif filetype == u"inc-man":
        return (config.file_prefix + config.file_prefix_manifest +
                b"duplicity-inc.%s.to.%s.vol%d.mandb.%s%s" %
                (dup_time.prevtimestr.encode(), dup_time.curtimestr.encode(),
                 volume_number, part_string, suffix))

    else:
        assert False, u"Invalid file type: %s" % filetype


def parse(filename):
    u"""
    Parse duplicity filename, return None or ParseResults object
    """
    def str2time(timestr, short):
        u"""
        Return time in seconds if string can be converted, None otherwise
        """
        if isinstance(timestr, bytes):
            timestr = timestr.decode()

        if short:
            t = from_base36(timestr)
        else:
            try:
                t = dup_time.genstrtotime(timestr.upper())
            except dup_time.TimeException:
                return None
        return t

    def get_vol_num(s, short):
        u"""
        Return volume number from volume number string
        """
        if short:
            return from_base36(s)
        else:
            return int(s)

    def set_encryption_or_compression(pr):
        u"""
        Set encryption and compression flags in ParseResults pr
        """
        if (filename.endswith(b'.z') or
                not config.short_filenames and filename.endswith(b'gz')):
            pr.compressed = True
        else:
            pr.compressed = False

        if (filename.endswith(b'.g') or
                not config.short_filenames and filename.endswith(b'.gpg')):
            pr.encrypted = True
        else:
            pr.encrypted = False

    # parse out the components and store in ParseResults
    pr = None
    prepare_regex()
    for fm in file_mapping:
        m = fm.regex.search(filename)
        if m:
            # get or supply volume number
            if fm.multivol:
                volume_number = get_vol_num(m.group(u"num"), fm.short)
            else:
                volume_number = 1

            # partial transfer?
            partial = m.group(u"partial") in [b".part", b".p"]

            # short filename?
            short = fm.short

            # full only has start time
            if fm.filetype.startswith(u'full'):
                t = str2time(m.group(u"time"), short)
                pr = ParseResults(fm.filetype, time=t,
                                  volume_number=volume_number,
                                  partial=partial)

            # inc has both start and end times
            else:
                t1 = str2time(m.group(u"start_time"), short)
                t2 = str2time(m.group(u"end_time"), short)
                pr = ParseResults(fm.filetype, start_time=t1, end_time=t2,
                                  volume_number=volume_number,
                                  partial=partial)
            break

    if pr:
        set_encryption_or_compression(pr)

    return pr


class ParseResults:
    u"""
    Hold information taken from a duplicity filename
    """
    def __init__(self, filetype, volume_number=None,
                 time=None, start_time=None, end_time=None,
                 encrypted=None, compressed=None, partial=False):

        assert volume_number
        assert filetype in [
            u"full-man",
            u"full-diff",
            u"full-sig",
            u"inc-man",
            u"inc-diff",
            u"inc-sig",
        ]

        self.filetype = filetype
        if filetype.startswith(u"inc"):
            assert start_time and end_time
        else:
            assert time

        self.volume_number = volume_number
        self.time = time
        self.start_time, self.end_time = start_time, end_time

        self.compressed = compressed  # true if gzip compressed
        self.encrypted = encrypted  # true if gpg encrypted

        self.partial = partial

    def is_full(self):
        self.filetype.startswith(u"full")

    def is_inc(self):
        self.filetype.startswith(u"inc") or self.filetype.startswith(u"new")

    def is_manifest(self):
        self.filetype.endswith(u"man")

    def is_difftar(self):
        self.filetype.endswith(u"diff")

    def is_sigtar(self):
        self.filetype.endswith(u"sig")

    def __eq__(self, other):
        return self.filetype == other.filetype and \
            self.manifest == other.manifest and \
            self.time == other.time and \
            self.start_time == other.start_time and \
            self.end_time == other.end_time and \
            self.partial == other.partial
